<img src="images/IDSNlogo.png" width="200">

# Final Project: Hands-on lab-Budget Allocation Application

## Estimated Time: 60 minutes

Note: Though this lab is split into multiple parts, it is highly recommended it is all completed in one sitting as the project environment may not be persisted. If for any reason that is not possible, please push your changes to git so that the changes are copied to your remote repository and you can start where you left.

## Pre-requisite: You need to complete Practice project before you start with the final project.

## Part A: Git clone the Git repository to have the react code you need to start

1. Open a terminal window by using the menu in the editor: Terminal > New Terminal.

<img src="images/new-terminal.png" width="75%"/>

**2.** Clone the repository you cloned and gitpushed in the practice project with all the components added by running the command given below:

``` 
git clone "git url"
```

**3.** This will clone the repository with budget allocation app files in your home directory in the lab environment. Check the same with the following command.

```
ls
```

**4.** Change to the project directory and check its contents.

```
cd budgetappreact && ls
```

**5.** All packages are needed to be installed are listed in package.json. Run npm install -s, to install and save those packages.

```
npm install  -s
```
## Task 1: Add the validation of the budget allocation.

## Task 2: Update the code to make Budget editable with increasing and decreasing its value by 10 tag.

## Task 3: Update the code to change the currency along with the drop-down option.Add these four currencies: $=Dollar,£=Pound,€=Euro,₹=Ruppee 

## Task 4: Add increase and decrease by 10 button in the allocation.

Your final React budget allocation application page should look like this:

<img src="images/final.PNG" width="75%"/> 

Congratulations! You have completed this Final Project lab and know how to create a budget allocation application.

**Change Log**

| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2022-08-31 | 1.0 | Pallavi Rai | Initial version created |

## <h3 align="center"> © IBM Corporation 2022. All rights reserved. <h3/>


