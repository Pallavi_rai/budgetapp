<img src="images/IDSNlogo.png" width="200">

# Practice Hands-on lab: Budget Allocation Application

## Estimated Time: 60 minutes

In this React Budget Allocation app, you will learn learn how to break down a UI into React components. You will become familiar with state using the Context API. Furthermore, you will explore actions, reducers, and the dispatch function.You will create a code file, save it, and edit it to make changes.

## Objectives:

- Setup a React Project.
- Put the UI Components in Place.
- Add the Context API.
- Render the created components and context in App.js.
- View your app on the browser.

## Exercise 1: Setup a React Project

**Clone the Git repository to have the react code you need to start**

**1.** Open a terminal window by using the menu in the editor: Terminal > New Terminal.

<img src="images/new-terminal.png" width="75%"/> 

**2.** Clone the repository by running the command given below:

``` 
git clone https://github.com/Pallavi7597/budgetappreact.git

```
**3.** This will clone the repository with budget allocation app files in your home directory in the lab environment. Check the same with the following command.

```
ls
```

**4.** Change to the project directory and check its contents.

```
cd budgetappreact && ls
```

**5.** All packages are needed to be installed are listed in package.json. Run npm install -s, to install and save those packages.

```
npm install  -s
```

## Exercise 2: Put the UI Components in Place

**1.** Make changes to **Budget.js** component. Jump into the code, in the `src/components/Budget.js`. Copy the below code and paste in the Budget.js

```
import React, { useContext } from 'react';
import { AppContext } from '../context/AppContext';

const Budget = () => {
	const { budget } = useContext(AppContext);

	return (
		<div className='alert alert-secondary'>
			<span>Budget: £{budget}</span>
		</div>
	);
};

export default Budget;
```
{: codeblock}

Here, you are using the Bootstrap Alert classes to gives a nice gray background. Adding some text and hard coding a value.

**2.** Make changes to **Remaining.js** component. Jump into the code, in the `src/components/Remaining.js`. Copy the below code and paste in the Remaining.js

```
import React, { useContext } from 'react';
import { AppContext } from '../context/AppContext';

const Remaining = () => {
	const { expenses, budget } = useContext(AppContext);

	const totalExpenses = expenses.reduce((total, item) => {
		return (total = total + item.cost);
	}, 0);

	const alertType = totalExpenses > budget ? 'alert-danger' : 'alert-success';

	return (
		<div className={`alert ${alertType}`}>
			<span>Remaining: £{budget - totalExpenses}</span>
		</div>
	);
};

export default Remaining;
```
{: codeblock}

Here, you are importing expenses and budget from Context. Getting the total cost of the expenses using the reduce function. Creating a variable to store the CSS classname you want to display. Using a template string to create your classes. Rendering the remaining budget using a subtraction.

**3.** Make changes to **ExpenseTotal.js** component. Jump into the code, in the `src/components/ExpenseTotal.js`. Copy the below code and paste in the ExpenseTotal.js.

```
import React, { useContext } from 'react';
import { AppContext } from '../context/AppContext';

const ExpenseTotal = () => {
	const { expenses } = useContext(AppContext);

	const totalExpenses = expenses.reduce((total, item) => {
		return (total += item.cost);
	}, 0);

	return (
		<div className='alert alert-primary'>
			<span>Spent so far: £{totalExpenses}</span>
		</div>
	);
};

export default ExpenseTotal;
```
{: codeblock}

Here, you are importing your useContext and AppContext. Taking the expenses from state. Using the reduce function to get a total of all the costs and assigning this to a variable. Displaying the variable in your JSX.
Now whenever the user adds an expense, this causes the state to update, which will cause all components connected to the context to re-render and update themselves with new values.

**4.** Make changes to **ExpenseList.js** component. Jump into the code, in the `src/components/ExpenseList.js`. Copy the below code and paste in the ExpenseList.js.

```
import React, { useContext } from 'react';
import ExpenseItem from './ExpenseItem';
import { AppContext } from '../context/AppContext';

const ExpenseList = () => {
	const { expenses } = useContext(AppContext);
	
	return (
		<table className='table'>
			  <thead className="thead-light">
		    <tr>
		      <th scope="col">Department</th>
		      <th scope="col">Allocated Budget</th>
		      <th scope="col">Delete</th>
		    </tr>
		  </thead>
			<tbody>
			{expenses.map((expense) => (
				<ExpenseItem id={expense.id} key={expense.id} name={expense.name} cost={expense.cost} />
			))}
			</tbody>
		</table>
	);
};

export default ExpenseList;
```
{: codeblock}

Here, we are creating a list, using the map function to iterate over the expenses and displaying an ExpenseItem component.


**5.** Make changes to **ExpenseItem.js** component. Jump into the code, in the `src/components/ExpenseItem.js`. Copy the below code and paste in the ExpenseItem.js.



```
import React, { useContext } from 'react';
import { TiDelete } from 'react-icons/ti';
import { AppContext } from '../context/AppContext';

const ExpenseItem = (props) => {
	const { dispatch, currency } = useContext(AppContext);

	const handleDeleteExpense = () => {
		dispatch({
			type: 'DELETE_EXPENSE',
			payload: props.id,
		});
	};

	const increaseAllocation = (name) => {
		const expense = {
			name: name,
			cost: 10,
		};

		dispatch({
			type: 'ADD_EXPENSE',
			payload: expense
		});

	}


	return (
		<tr>
		<td>{props.name}</td>
		<td>{currency}{props.cost}</td>
		<td align="center"><button onClick={event=> increaseAllocation(props.name)}>+</button></td>
		<td align="center"><TiDelete size='1.5em' onClick={handleDeleteExpense}></TiDelete></td>
		</tr>
	);
};

export default ExpenseItem;
```
{: codeblock}

Here, you are importing dispatch from Context, which allows you to dispatch a delete action. Creating a function that gets called when the delete icon is clicked. Dispatching an action. Your action contains the type (so the reducer knows how to update the state) and the payload. In this case you're passing the ID of this expense (which you get from props when you rendered the ExpenseList)

**6.** Make changes to **AllocationForm.js** component. Jump into the code, in the `src/components/AllocationForm.js`. Copy the below code and paste in the AllocationForm.js.

```
import React, { useContext, useState } from 'react';
import { AppContext } from '../context/AppContext';

const AllocationForm = (props) => {
	const { dispatch,remaining  } = useContext(AppContext);

	const [name, setName] = useState('');
	const [cost, setCost] = useState('');
	const [action, setAction] = useState('');

	const submitEvent = () => {

			if(cost > remaining) {
				alert("The value cannot exceed remaining funds  £"+remaining);
				setCost("");
				return;
			}

		const expense = {
			name: name,
			cost: parseInt(cost),
		};
		if(action === "Reduce") {
			dispatch({
				type: 'RED_EXPENSE',
				payload: expense,
			});
		} else {
				dispatch({
					type: 'ADD_EXPENSE',
					payload: expense,
				});
			}
	};

	return (
		<div>
			<div className='row'>
				
			<div className="input-group mb-3" style={{ marginLeft: '2rem' }}>
					<div className="input-group-prepend">
		    	<label className="input-group-text" htmlFor="inputGroupSelect01">Department</label>
		  		</div>
  				<select className="custom-select" id="inputGroupSelect01" onChange={(event) => setName(event.target.value)}>
						<option defaultValue>Choose...</option>
						<option value="Marketing" name="marketing"> Marketing</option>
	        	<option value="Sales" name="sales">Sales</option>
	        	<option value="Finance" name="finance">Finance</option>
	        	<option value="HR" name="hr">HR</option>
	        	<option value="IT" name="it">IT</option>
	        	<option value="Admin" name="admin">Admin</option>
				  </select>
					
					<div className="input-group-prepend" style={{ marginLeft: '2rem' }}>
		    	<label className="input-group-text" htmlFor="inputGroupSelect02">Allocation</label>
		  		</div>
  				<select className="custom-select" id="inputGroupSelect02" onChange={(event) => setAction(event.target.value)}>
						<option defaultValue value="Add" name="Add">Add</option>
	        	<option value="Reduce" name="Reduce">Reduce</option>
				  </select>

					<input
						required='required'
						type='number'
						id='cost'
						value={cost}
						style={{ marginLeft: '2rem' , size: 10}}
						onChange={(event) => setCost(event.target.value)}>
						</input>

					<button className="btn btn-primary" onClick={submitEvent} style={{ marginLeft: '2rem' }}>
						Save
					</button>
				</div>
				</div>
				
		</div>
	);
};

export default AllocationForm;
```
{: codeblock}

Here, you are adding form tags. Adding a label/input for name, cost and action field. Adding values for various departments.


## Excercise 3: Add the Context API

**1.** Make changes to **AppContext.js** context. Jump into the code, in the `src/contexts/AppContext.js`. Copy the below code and paste in the AppContext.js.

```
import React, { createContext, useReducer } from 'react';

// 5. The reduceer - this is used to update the state, based on the action
export const AppReducer = (state, action) => {
	let budget = 0;
	switch (action.type) {
		case 'ADD_EXPENSE':
			let total_budget = 0;
			total_budget = state.expenses.reduce(
				(previousExp, currentExp) => {
					return previousExp + currentExp.cost
				},0
			);
			total_budget = total_budget + action.payload.cost;
			action.type = "DONE";
			if(total_budget <= state.budget) {
				total_budget = 0;
				state.expenses.map((currentExp)=> {
					if(currentExp.name === action.payload.name) {
						currentExp.cost = action.payload.cost + currentExp.cost;
					}
					return currentExp
				});
				return {
					...state,
				};
			} else {
				alert("Cannot increase the allocation! Out of funds");
				return {
					...state
				}
			}
			case 'RED_EXPENSE':
				const red_expenses = state.expenses.map((currentExp)=> {
					if (currentExp.name === action.payload.name && currentExp.cost - action.payload.cost >= 0) {
						currentExp.cost =  currentExp.cost - action.payload.cost;
						budget = state.budget + action.payload.cost
					}
					return currentExp
				})
				action.type = "DONE";
				return {
					...state,
					expenses: [...red_expenses],
				};
			case 'DELETE_EXPENSE':
			action.type = "DONE";
			state.expenses.map((currentExp)=> {
				if (currentExp.name === action.payload) {
					budget = state.budget + currentExp.cost
					currentExp.cost =  0;
				}
				return currentExp
			})
			action.type = "DONE";
			return {
				...state,
				budget
			};
		case 'SET_BUDGET':
			action.type = "DONE";
			state.budget = action.payload;

			return {
				...state,
			};
		case 'CHG_CURRENCY':
			action.type = "DONE";
			state.currency = action.payload;
			return {
				...state
			}

		default:
			return state;
	}
};

// 1. Sets the initial state when the app loads
const initialState = {
	budget: 2000,
	expenses: [
		{ id: "Marketing", name: 'Marketing', cost: 50 },
		{ id: "Finance", name: 'Finance', cost: 300 },
		{ id: "Sales", name: 'Sales', cost: 70 },
		{ id: "Human Resource", name: 'Human Resource', cost: 40 },
		{ id: "IT", name: 'IT', cost: 500 },
	],
	currency: '£'
};

// 2. Creates the context this is the thing our components import and use to get the state
export const AppContext = createContext();

// 3. Provider component - wraps the components we want to give access to the state
// Accepts the children, which are the nested(wrapped) components
export const AppProvider = (props) => {
	// 4. Sets up the app state. takes a reducer, and an initial state
	const [state, dispatch] = useReducer(AppReducer, initialState);
	let remaining = 0;

	if (state.expenses) {
			const totalExpenses = state.expenses.reduce((total, item) => {
			return (total = total + item.cost);
		}, 0);
		remaining = state.budget - totalExpenses;
	}

	return (
		<AppContext.Provider
			value={{
				expenses: state.expenses,
				budget: state.budget,
				remaining: remaining,
				dispatch,
				currency: state.currency
			}}
		>
			{props.children}
		</AppContext.Provider>
	);
};
```
{: codeblock}

Here, you are adding an initial budget.Creating a Provider component, setting up the useReducer hook which will hold your state, and allow you to update the state via dispatch.

Adding a new case to the switch statement called "ADD_EXPENSE", "RED_EXPENSE", "DELETE_EXPENSE".

## Excercise 4: Render the craeted components and context in App.js

**1.** Make changes to **App.js**. Jump into the code, in the `src/App.js`. Copy the below code structure and paste in the App.js. You need to add the code in the container div to import and add the components created above.

```
import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
// Add code to import the components
import { AppProvider } from './context/AppContext';
const App = () => {
    return (
        <AppProvider>
            <div className='container'>
                {/* Add the code here to add the components          */}
            </div>
        </AppProvider>
    );
};
export default App;
```
{: codeblock}

Here, you are importing different components.Adding a bootstrap container that helps you center your App on the page 
- Adding a title
- Adding a Bootstrap row
- Adding a column within the row for each of your components so far
- Imported and Rendered the AllocationForm 
- Imported AppProvider and Nested components in the AppProvider element.

## Excercise 5: Launch and view your react app on the browser

**1.** Make sure you are in the `budgetappreact` directory and run the server using the following command.

```
npm start
```

**2.** Click on the Skills Network button on the left, it will open the **"Skills Network Toolbox"**. Then click the **Other** then **Launch Application**. From there you should be able to enter the port `3000`.

<img src="images/Launch_Application-SN-ToolBox.png" width="75%"/> 

**3.** Verify your output on the browser. 

<img src="images/bugdetapp.PNG" width="75%"/> 


## Commit and push your local code to your remote git repository

This git pushed code will be cloned and used in final project.

Please refer to this lab [Working with git in the Theia lab environment](https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBMDeveloperSkillsNetwork-CD0101EN-SkillsNetwork/labs/GitHubLabs/Github_commit.md.html)


Congratulations! You have completed this practice lab and know how to create components of a budget allocation application.

**Change Log**

| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2022-08-31 | 1.0 | Pallavi Rai | Initial version created |

## <h3 align="center"> © IBM Corporation 2022. All rights reserved. <h3/>
















